﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassificationSystem
{
    class Test
    {
        static void Main(string[] args)
        {
            foreach (var phrase in new String[] {
                "Conhecido em parte o recurso de MARIA HELENA MELE MORGAN - CPF: 584.230.441-49 (AGRAVANTE) e provido",
                "Juntada de Petição de outras peças",
                "MANDADO: DEVOLVIDO / CUMPRIDO CITACAO, PENHORA E AVALIACAO",
                "Admissão de Incidente de Resolução de Demandas Repetitivas.",
                "Admissão de Incidente de Resolução de Demandas Repetitivas"
            })
            {
                var testResult = ClassificationSystem.Classificate(phrase);
                Console.WriteLine(testResult);
            }
        }
    }
}
