﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace ClassificationSystem
{
    public class Rule
    {
        public String Class;
        public String Name;
        public List<String> CheckedList;
        public List<String> PhraseList;
        public List<String> FixedPhraseList;
    }

    public static class ClassificationSystem
    {
        private static readonly String JSON_PATH = Path.Combine(Environment.CurrentDirectory, "rulesList.json");
        private static readonly List<Rule> RulesList = new Func<List<Rule>>(() => {
            List<Rule> rulesList = new List<Rule>();

            using (StreamReader reader = new StreamReader(JSON_PATH))
            {
                String rulesJson = reader.ReadToEnd();
                dynamic rulesArray = JsonConvert.DeserializeObject(rulesJson);

                foreach (var rule in rulesArray)
                {
                    rulesList.Add(new Rule
                    {
                        Class = rule["class"],
                        Name = rule.name,
                        CheckedList = new List<dynamic>(rule.checkeds).Select(item => (String)item).ToList(),
                        PhraseList = new List<dynamic>(rule.phrase).Select(value => (String)value).ToList(),
                        FixedPhraseList = new List<dynamic>(rule.fixed_phrase).Select(value => (String)value).ToList()
                    });
                }
            }

            return rulesList;
        })();

        static string RemoveDiacritics(string text)
        {
            var normalizedString = text.Normalize(NormalizationForm.FormD);
            var stringBuilder = new StringBuilder();

            foreach (var c in normalizedString)
            {
                var unicodeCategory = CharUnicodeInfo.GetUnicodeCategory(c);
                if (unicodeCategory != UnicodeCategory.NonSpacingMark)
                {
                    stringBuilder.Append(c);
                }
            }

            return stringBuilder.ToString().Normalize(NormalizationForm.FormC);
        }

        public static String NormalizePhrase(String phrase)
        {
            phrase = phrase.ToLower(); // Minúsculo
            phrase = Encoding.UTF8.GetString(Encoding.GetEncoding("ISO-8859-8").GetBytes(phrase));// Removendo pontuação - https://stackoverflow.com/a/2086575/8645424
            if (phrase.EndsWith(".") && !phrase.EndsWith("...")) // Caso a frase termine em Ponto Final, removeremos ele
            {
                phrase = phrase.Remove(phrase.Length - 1);
            }

            phrase = phrase.Replace("/", "");
            phrase = phrase.Replace("-", "");
            phrase = phrase.Replace(":", "");
            phrase = phrase.Replace("(a)", "a");
            phrase = phrase.Replace("(o)", "o");
            phrase = phrase.Replace("eletroinica", "eletronica");
            phrase = phrase.Trim();
            phrase = String.Join(" ", phrase.Split(' ').Where(c => c != "")); // Removendo os espaços em branco que estão repetidos dentro da frase

            return phrase;
        }

        public static String Classificate(String movementDescription)
        {
            var normalizedPhrase = NormalizePhrase(movementDescription);
            var matchedRules = new List<Rule>();
            foreach (var rule in RulesList)
            {
                foreach (var fixedPhraseOriginal in rule.FixedPhraseList)
                {
                    var fixedPhrase = fixedPhraseOriginal.Replace("(a)", "a").Replace("(o)", "o");
                    if (Regex.IsMatch(normalizedPhrase, $"^{fixedPhrase}$") && !matchedRules.Contains(rule))
                    {
                        matchedRules.Add(rule);
                    }
                }
            }

            String classCode = null;

            // Frase exemplo: Juntada de Petição de outras peças
            // Nesse exemplo, a frase dará match com duas classes: "Juntada de #{tipo_de_documento}" e "Juntada de Petição de #{tipo_de_peticao}"
            // Com isso, forçamos a escolha da segunda classe, que é a correta
            if (
                matchedRules.Count == 2 &&
                matchedRules.Count(r => new String[2] { "581", "85" }.Contains(r.Class)) == 2 &&
                NormalizePhrase(movementDescription).StartsWith(NormalizePhrase("Juntada de Petição de "))
            )
            {
                classCode = "85";
            }
            // Frase exemplo: MANDADO: DEVOLVIDO / CUMPRIDO CITACAO, PENHORA E AVALIACAO
            // A frase dará match com duas classes: "mandado devolvido cumprido citacao, penhora e avaliacao" e "mandado devolvido .*"
            // O primeiro caso é o correto
            else if (
                matchedRules.Count == 2 &&
                matchedRules.Count(r => new String[2] { "11382", "106" }.Contains(r.Class)) == 2 &&
                NormalizePhrase(movementDescription).StartsWith(NormalizePhrase("MANDADO: DEVOLVIDO"))
            )
            {
                classCode = "11382";
            }
            else if (matchedRules.Count == 1) // Caso tenha encontrado apenas uma regra, iremos retornar a regra encontrada
            {
                classCode = matchedRules[0].Class;
            }
            else if (matchedRules.Count > 1) // Se por algum motivo haver match com mais de uma regra, algo está errado
            {
                throw new Exception($"Um match não tratado de {matchedRules.Count } regras foi encontrado!");
            }

            return classCode;
        }
    }
}